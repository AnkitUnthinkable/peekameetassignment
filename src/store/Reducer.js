import * as actionTypes from "./ActionTypes";

const initialState = {
  token: null,
  loading: false,
  userId: null,
  error: null,
  authenticated: false,
  noteData: []
};

const authReducer = (state = initialState, action) => {
  let UpdatedState;

  switch (action.type) {
    case actionTypes.AUTH_START:
      UpdatedState = { ...state, loading: true };
      return UpdatedState;

    case actionTypes.AUTH_SUCCESS:
      UpdatedState = {
        ...state,
        loading: false,
        token: action.token,
        userId: action.userId,
        authenticated: true
      };
      console.log(UpdatedState,"updated state")
      return UpdatedState;

    case actionTypes.AUTH_FAIL:
      UpdatedState = { ...state, loading: false, error: action.error };
      return UpdatedState;

    case actionTypes.NOTE_DATA:
      UpdatedState = { ...state, noteData: [...action.noteData] };
      return UpdatedState;

    case actionTypes.DELETE_NOTE:
      return state;

    default:
      return state;
  }
};

export default authReducer;
