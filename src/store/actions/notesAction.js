import * as actionTypes from "../ActionTypes";
import {
  getNoteData,
  deleteNotes,
  addNotes,
  editNotes
} from "../../apiEndPoint/ApiEndPoint";

export const noteData = () => {
  return dispatch => {
    getNoteData({
      query: { createdFor: localStorage.getItem("id") },
      headers: { Authorization: localStorage.getItem("token") }
    })
      .then(response => {
        return dispatch({
          type: actionTypes.NOTE_DATA,
          noteData: response.data.data[0].docs
        });
      })
      .catch(err => {
        console.log(err);
        return { type: actionTypes.NOTE_DATA, noteData: err };
      });
  };
};

export const deleteNote = noteId => {
  return dispatch => {
    deleteNotes(
      noteId,
      { headers: { Authorization: localStorage.getItem("token") } },
      { noteId: noteId }
    )
      .then(response => {
        dispatch({
          type: actionTypes.DELETE_NOTE
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const addNote = (payLoad, header) => {
  return dispatch => {
    addNotes(payLoad, header)
      .then(response => {
        console.log(response);
      })
      .catch(err => {
        console.log(err);
      });
  };
};
export const editNote = (noteId, payLoad, header) => {
    console.log(noteId+" "+payLoad+" "+header)
  return dispatch => {
    editNotes(noteId, payLoad, header)
      .then(response => {
        console.log(response);
      })
      .catch(err => {
        console.log(err);
      });
  };
};
