import React from 'react'
import LogoImage from '../../../component/logo/logoimage/LogoImage'
import LogoTitle from '../../../component/logo/logotitle/LogoTitle'
import Logo from '../../../component/logo/Logo'
import  './LoginHeader.css'
import Button from '../button/Button'

const LoginHeader = () => {
    return(
        <div className="Background">
            <div className ="Logo">
                < Logo />
            </div>  
            <Button className = "Div Button" name="Sign In"/> 
            <Button className = "Div Button" name="Sign Up"/> 
        </div>
    )
}
export default LoginHeader;