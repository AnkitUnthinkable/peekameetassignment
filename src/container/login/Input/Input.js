import React from 'react';
import './Input.css';

const Input = (props) => {
    return (
        <div>
            <div className="input">
                <label>{props.label}</label>
                <br />
                <input type={props.type} name={props.name} label={props.label} onChange={props.onChange}></input>
                <div className="error">{props.error}</div>
            </div>
        </div>
    );
}

export default Input;