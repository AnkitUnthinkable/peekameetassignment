import React from 'react';
import './RoundContainer.css';

const RoundContainer = (props) => {

    return (
        <section className="RoundContainer"><p>{props.data}</p></section>
    );
};
export default RoundContainer;