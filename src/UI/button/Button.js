import React from "react";
import "./Button.css";

const Button = props => {
  return (
    <div className="Button">
      <img src={props.buttonIcon} alt='share' />
      <p>{props.children}</p>
    </div>
  );
};

export default Button;
