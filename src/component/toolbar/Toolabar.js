import React from "react";
import   "./Toolbar.css";
import Logo from "../logo/Logo";
import NavigationItems from "../navigation/navigationItems/NavigationItems";
import menuBtn from "../../assets/menu.svg";
import { connect } from "react-redux";
import Aux from "../../auxWrapper/AuxWrapper";

const Toolbar = props => {
  let toolbar = null;
  if (localStorage.getItem('token')) {
    toolbar = (
      <Aux>
        <nav>
          <NavigationItems/>
        </nav>
        <img src={menuBtn} alt='Menu Button' />
        <button onClick={props.logout}>LOGOUT</button> 
      </Aux>
    );
  }
  return (
    <header className="Header">
      <Logo />  {toolbar}
    </header>
  );
};
const mapStatetoProps = state => {
  return {
    authenticated: state.authenticated
  };
};

export default connect(mapStatetoProps)(Toolbar);