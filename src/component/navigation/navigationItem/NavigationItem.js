import React from "react";
import { Link} from "react-router-dom";

const NavigationItem = props => {
  return (
 
      <li>
        <img src={props.image} alt='Home' />
        <Link to="#">{props.children}</Link>
      </li>
 
  );
};
export default NavigationItem;
