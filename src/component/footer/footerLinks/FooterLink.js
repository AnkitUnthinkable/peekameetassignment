import React, { Component } from 'react';
import './FooterLink.css';

class FooterLink extends Component {

    render() {
        return (
            <span className="textContent">
                {this.props.textContent}
            </span>
        )

    }
}

export default FooterLink;