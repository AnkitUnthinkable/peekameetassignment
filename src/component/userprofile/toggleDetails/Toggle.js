import React, { Component } from "react";
import  "./Toggle.css";
import { NavLink, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as actionCreator from '../../../store/actions/notesAction';

class Toggle extends Component {
  render() {
    return (
      <div className="Toggle">
        <NavLink to='/user'>{this.props.details}</NavLink>
        <a href='/notes' onClick={this.noteData}>
          {this.props.notes}
        </a>
      </div>
    );
  }
  noteData = event => {
    event.preventDefault();
    this.props.noteData();
    this.props.history.push("/user/notes");
  };
}

const mapDispatchToprops = dispatch => {
  return {
    noteData: () => dispatch(actionCreator.noteData())
  };
};

export default connect(null, mapDispatchToprops)(withRouter(Toggle));
