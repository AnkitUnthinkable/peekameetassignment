import React from "react";
import './ProfilePic.css';
const ProfilePic = (props) => {
  return <div className="ProfilePic"><img src={props.profileImage.url} alt='Profile Pic' /></div>;
};

export default ProfilePic;
