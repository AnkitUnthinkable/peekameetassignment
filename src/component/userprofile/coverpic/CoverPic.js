import React from "react";
import coverpic from "../../../assets/cover.png";
import './CoverPic.css';
const CoverPic = () => {
  return<div className="CoverPic"> <img src={coverpic} alt='Cover Pic' /></div>;
};

export default CoverPic;
