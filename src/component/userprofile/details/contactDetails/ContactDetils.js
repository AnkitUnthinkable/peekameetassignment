import React from "react";
import "./ContactDetails.css";

const ContactDetails = (props) => {
  return( 
  <div className="ContactDetails">
      <img src={props.contactDetailIcon} alt="icon"/>
      <p>{props.contactDetails}</p>
  </div>);
};

export default ContactDetails;
