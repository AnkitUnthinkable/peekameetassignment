import React from "react";
import HomePage from "./layout/Layout";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import reducer from "./store/Reducer";
import thunk from 'redux-thunk'
import LoginPage from './container/login/LoginPage'


const store = createStore(reducer, applyMiddleware(thunk));

function App(props) {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={LoginPage} />
          <Route path='/user' component={HomePage} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}
export default App;
