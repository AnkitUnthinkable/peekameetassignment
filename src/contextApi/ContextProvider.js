import React, { Component } from "react";
import { getUserProfileData } from "../apiEndPoint/ApiEndPoint";

export const UseContext = React.createContext();

class ContextProvider extends Component {
  state = {};

  componentDidMount() {


    //////////////////API CALL
    if (localStorage.getItem("token")) {
      getUserProfileData({
        params: { userId: localStorage.getItem("id") },
        headers: { Authorization: localStorage.getItem("token") }
      })
        .then(response => {
          this.setState({
            ...response.data.data[0]
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  }
  render() {
    return (
      <UseContext.Provider value={this.state}>
        {this.props.children}
      </UseContext.Provider>
    );
  }
}
export default ContextProvider;
